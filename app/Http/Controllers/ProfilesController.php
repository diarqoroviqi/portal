<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfilesController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        return view('admin.users.profile', compact('user'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
           'name' => 'required',
           'email' => 'required',
           'facebook' => 'required|url',
           'youtube' => 'required|url',
        ]);

        $user = auth()->user();

        if ($request->hasFile('avatar'))
        {
            $avatar = $request->avatar;

            $avatar_new_name = time() . $avatar->getClientOriginalName();

            $avatar->move('uploads/avatars', $avatar_new_name);

            $user->profile->avatar = 'uploads/avatars/'.$avatar_new_name;

            $user->profile->save();
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->profile->facebook = $request->facebook;
        $user->profile->youtube = $request->youtube;
        $user->profile->about = $request->about;

        $user->update();
        $user->profile->update();

        if ($request->has('password'))
        {
            $user->password = bcrypt($request->password);
            $user->save();
        }

        session()->flash('success', 'Account profile updated');
        return redirect()->back();
    }

}
