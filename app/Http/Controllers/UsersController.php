<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }


    public function index()
    {
        $users = User::all();
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
           'name'=> 'required',
           'email'=> 'required|email',
        ]);

        $user = User::create([
           'name' => $request->name,
           'email' => $request->email,
           'password' => bcrypt('password')

        ]);

        $profile = Profile::create([
           'user_id' => $user->id,
            'avatar' => 'uploads/avatars/avatar4.png'
        ]);

        session()->flash('success', 'User Added successfully');
        return redirect()->route('users.index');
    }

    public function admin($id)
    {
        $user = User::find($id);

        $user->admin = 1;
        $user->save();
        session()->flash('success', 'Successfully changed user permissions');
        return redirect()->back();
    }

    public function not_admin($id)
    {
        $user = User::find($id);

        $user->admin = 0;
        $user->save();
        session()->flash('success', 'Successfully changed user permissions');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $user = User::find($id);

        $user->profile->delete();
        $user->delete();
        session()->flash('success', 'User deleted forever');
        return redirect()->back();
    }
}
