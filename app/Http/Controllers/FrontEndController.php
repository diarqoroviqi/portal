<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Setting;
use App\Tag;
use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    public function index()
    {
        $title = Setting::first()->site_name;
        $categories = Category::take(5)->get();
        $first_post = Post::orderBy('created_at', 'desc')->first();
        $second_post = Post::orderBy('created_at', 'desc')->skip(1)->take(1)->get()->first();
        $third_post = Post::orderBy('created_at', 'desc')->skip(2)->take(1)->get()->first();
        $php = Category::find(6);
        $laravel = Category::find(7);
        $settings = Setting::first();

        return view('index',
            compact(
                'title',
                'categories',
                'first_post',
                'second_post',
                'third_post',
                'php',
                'laravel',
                'settings'
        ));
    }

    public function singlePost($slug){

        $post = Post::where('slug', $slug)->first();
        $tags= Tag::all();

        $next_id = Post::where('id' ,'>', $post->id)->min('id');
        $next = Post::find($next_id);

        $prev_id = Post::where('id', '<', $post->id)->max('id');
        $prev = Post::find($prev_id);

        $title = $post->title;
        $categories = Category::take(5)->get();
        $settings = Setting::first();
        return view('single', compact('post', 'title', 'categories', 'settings', 'next', 'prev', 'tags'));
    }

    public function category($id)
    {
        $category = Category::find($id);
        $title = $category->name;

        $settings = Setting::first();
        $categories = Category::take(5)->get();

        return view('category', compact('category', 'title', 'settings', 'categories'));
    }

    public function tag($id)
    {
        $tag = Tag::find($id);
        $title = $tag->tag;

        $settings = Setting::first();
        $categories = Category::take(5)->get();

        return view('tag', compact('tag', 'title', 'settings', 'categories'));
    }
}
