<?php

namespace App\Http\Controllers;

use App\Post;
use App\Category;
use App\Tag;
use Illuminate\Http\Request;

class PostsController extends Controller
{

    public function trashed()
    {

        $posts = Post::onlyTrashed()->get();
        return view('admin.posts.trashed', compact('posts'));
    }

    public function restore($id)
    {
        $post = Post::withTrashed()->restore();;

        session()->flash('success', 'The trashed post restored successfully');
        return redirect()->route('posts.index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();

        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags= Tag::all();
        if ($tags->count() == 0){
            session()->flash('info', 'You must have some tags before attempting to create a post.');
            return redirect()->back();
        }

        $categories = Category::all();
        if($categories->count() == 0){
            session()->flash('info', 'You must have some categories before attempting to create a post.');
            return redirect()->back();
        }
        return view('admin.posts.create', compact('categories', 'tags'));
    }

/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
           'title' => 'required|max:255',
           'featured' => 'required|image',
           'content' => 'required',
           'category_id' => 'required',
           'tags' => 'required'
        ]);

        $featured = $request->featured;

        $featured_new_name = time().$featured->getClientOriginalName();

        $featured->move('uploads/posts', $featured_new_name);

        $post = Post::create([
            'title' => $request->title,
            'content' => $request->content,
            'featured' => '/uploads/posts/'.$featured_new_name,
            'category_id' => $request->category_id,
            'slug' => str_slug($request->title),
            'user_id' => auth()->user()->id
        ]);

        $post->tags()->attach($request->tags);

        session()->flash('success', 'Post created successfully');
        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tags= Tag::all();
        $post = Post::find($id);
        $categories = Category::all();
        return view('admin.posts.edit', compact('post', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'title' => 'required',
            'content' => 'required',
            'category_id' => 'required',
        ]);

        $post=Post::find($id);

        if ($request->hasFile('featured'))
        {
            $featured = $request->featured;
            $featured_new_name = time().$featured->getClientOriginalName();
            $featured->move('uploads/posts', $featured_new_name);

            $post->featured = '/uploads/posts/'.$featured_new_name  ;
        }

        $post->title = $request->title;
        $post->content = $request->content;
        $post->category_id = $request->category_id;


        $post->update();

        $post->tags()->sync($request->tags);
        session()->flash('success', 'Post updatedsuccessfully');
        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        $post->delete();

        session()->flash('success', 'Your post was just trashed');
        return redirect()->back();
    }

    public function kill($id)
    {
        $post = Post::withTrashed()->where('id', $id)->first();
        $post->forceDelete();
        session()->flash('success', 'Post deleted permanently.');
        return redirect()->back();
    }


}
