@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Tags
        </div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <th>Tag name</th>
                <th>Editing</th>
                <th>Deleting</th>
                </thead>
                <tbody>
                @if($tags->count()>0)
                    @foreach($tags as $tag)
                        <tr>
                            <td>{{ $tag->tag }}</td>
                            <td>
                                <a href="{{ route('tags.edit', ['id' => $tag->id]) }}" class="btn btn-info btn-xs">
                                    Edit
                                </a>
                            </td>
                            <td>
                                <form action="{{ route('tags.destroy', ['id' => $tag->id]) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="submit" class="btn btn-danger btn-xs" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                <th>
                    <h3 class="text-center">No Tags yet</h3>
                </th>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection