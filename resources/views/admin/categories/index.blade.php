@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <th>Category name</th>
                <th>Editing</th>
                <th>Deleting</th>
                </thead>
                <tbody>
                @if($categories->count() > 0)
                    @foreach($categories as $category)
                        <tr>
                            <td>{{ $category->name }}</td>
                            <td>
                                <a href="{{ route('categories.edit', ['id' => $category->id]) }}" class="btn btn-info btn-xs">
                                  Edit
                                </a>
                            </td>
                            <td>
                                <form action="{{ route('categories.destroy', ['id' => $category->id]) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="submit" class="btn btn-danger btn-xs" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <th colspan="5" class="text-center">No categories yet</th>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection