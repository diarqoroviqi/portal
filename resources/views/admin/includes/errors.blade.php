@if(count($errors)>0)

    <ul class="list-group">
        @foreach($errors->all() as $error)
            <li style="color: red;" class="list-group-item">
                {{ $error }}
            </li>
        @endforeach
    </ul>

@endif