@extends('layouts.app')

@section('content')

    @include('admin.includes.errors')

    <div class="panel panel-default">
        <div class="panel-heading">
            Edit Portal settings
        </div>
        <div class="panel-body">
            <form action="{{ route('settings.update') }}" method="POST">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="site_name">Site name</label>
                    <input type="text" name="site_name" id="site_name" value="{{ $settings->site_name }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" name="address" id="address" class="form-control" value="{{ $settings->address }}">
                </div>

                <div class="form-group">
                    <label for="contact_number">Contact Phone</label>
                    <input type="text" name="contact_number" id="contact_number" class="form-control" value="{{ $settings->contact_number }}">
                </div>

                <div class="form-group">
                    <label for="contact_email">Contact Email</label>
                    <input type="email" name="contact_email" id="contact_email" class="form-control" value="{{ $settings->contact_email }}">
                </div>


                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit">Update site settings</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection