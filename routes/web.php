<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/subscribe', function (){
   $email = request('email');

   Newsletter::subscribe($email);

   session()->flash('subscribed', 'Successfully subsibed');
   return redirect()->back();
});

Route::get('/', [
    'uses' => 'FrontEndController@index',
    'as' => 'index'
]);

Route::get('/results', function(){
   $posts = \App\Post::where('title', 'like', '%'.request('query').'%')->get();
    $title = "Search results: ". request('query');
    $query = request('query');

    $settings = \App\Setting::first();
    $categories = \App\Category::take(5)->get();
   return view('results', compact('posts', 'title', 'settings', 'categories', 'query'));
});

Auth::routes();

Route::get('/test', function(){
   return App\User::find(1)->profile;
});

Route::get('/post/{slug}', [
    'uses' => 'FrontEndController@singlePost',
    'as' => 'post.single'
]);

Route::get('/category/{id}', [
   'uses' => 'FrontEndController@category',
    'as' => 'category.single'
]);

Route::get('/tag/{id}', [
    'uses' => 'FrontEndController@tag',
    'as' => 'tag.single'
]);





Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){

    Route::get('/dashboard', 'HomeController@index')->name('home');

    Route::get('/posts/trashed', [
        'uses' => 'PostsController@trashed',
        'as' => 'posts.trashed'
    ]);
    Route::get('/posts/restore/{id}', [
        'uses' => 'PostsController@restore',
        'as' => 'posts.restore'
    ]);

    Route::get('/posts/kill/{id}', [
        'uses' => 'PostsController@kill',
        'as' => 'posts.kill'
    ]);
    Route::resource('posts', 'PostsController');

    Route::resource('categories', 'CategoriesController');
    Route::resource('tags', 'TagsController');

    Route::get('/users', [
        'uses' => 'UsersController@index',
        'as' => 'users.index'
    ]);
    Route::get('/users/create', [
        'uses' => 'UsersController@create',
        'as' => 'users.create'
    ]);
    Route::post('/users', [
        'uses' => 'UsersController@store',
        'as' => 'users.store'
    ]);
    Route::get('/users/admin/{id}', [
        'uses' => 'UsersController@admin',
        'as' => 'users.admin'
    ])->middleware('admin');
    Route::get('/users/not-admin/{id}', [
        'uses' => 'UsersController@not_admin',
        'as' => 'users.not.admin'
    ]);

    Route::get('/users/profile', [
       'uses' => 'ProfilesController@index',
        'as' => 'users.profile'
    ]);

    Route::post('/users/profile', [
        'uses' => 'ProfilesController@update',
            'as' => 'users.profile.update'
    ]);
    Route::get('/users/delete/{id}', [
        'uses' => 'UsersController@destroy',
        'as' => 'users.delete'
    ]);

    Route::get('/settings', [
        'uses' => 'SettingsController@index',
        'as' => 'settings'
    ]);

    Route::post('/settings/update', [
        'uses' => 'SettingsController@update',
        'as' => 'settings.update'
    ]);
});



