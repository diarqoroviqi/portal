<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::create([
           'name' => 'diar qoroviqi',
            'email' => 'qoroviqidiar@hotmail.com',
            'password' => bcrypt('123456'),
            'admin' => 1
        ]);

        App\Profile::create([
            'user_id' =>$user->id,
            'avatar' => 'uploads/avatars/2.png',
            'about' => \Faker\Provider\Lorem::text(30),
            'facebook' => 'facebook.com',
            'youtube' => 'youtube.com'
        ]);
    }
}
