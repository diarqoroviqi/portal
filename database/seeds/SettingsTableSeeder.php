<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Setting::create([
           'site_name' => "Laravel's Portal",
            'address' => 'Kosova, Mitrovica',
            'contact_number' => '+38649169082',
            'contact_email' => 'qoroviqidiar@gmail.com',
        ]);
    }
}
